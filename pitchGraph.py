import pygame
import numpy
import math
import pitch

(width, height) = (768, 512)



keyPattern = (0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0)
bufferSize=width-32
pitchBuffer = [None]*bufferSize
beatBuffer = [None]*bufferSize
graphBounds = pygame.Rect(16, 64, width-32, height-128)

pitchRange = [-24, 0] #semitones relative to A
startOctave = pitch.linearPitchToTone(pitchRange[0])[0]
endOctave = pitch.linearPitchToTone(pitchRange[1])[0]

pitchRange[0]-=pitch.linearPitchToTone(pitchRange[0])[1]
pitchRange[1]-=pitch.linearPitchToTone(pitchRange[1])[1]-12
octaves=endOctave-startOctave+1
pitchRangeCoordinates = (graphBounds.y + graphBounds.height/octaves/24,
    graphBounds.y + graphBounds.height - graphBounds.height/octaves/24)

MIN_LOUDNESS=0

likelyChord=""

def init():
    global width
    global height
    global screen

    pygame.init()
    screen = pygame.display.set_mode((width,height))
    pygame.display.set_caption('aRHYTHMetica')
    screen.fill((100, 100, 100))
    drawScale();
    pygame.display.update()


def end():
    pygame.display.quit() # so that the display does not stay there forever


def pushData(pitch, loudness, beat):
    global MIN_LOUDNESS
    if loudness<MIN_LOUDNESS:
        newPitch=None
    else:
        newPitch=getCoordinate(pitch)

    global screen
    global pitchBuffer

    del pitchBuffer[0]
    pitchBuffer.append(newPitch)
    del beatBuffer[0]
    beatBuffer.append(beat)

    draw()
    #pygame.draw.circle(screen, (255, 255, 255), (x, int(y)), 10)

def setChord(beat, chord):
    print "setChord ", beat, chord
    global beatBuffer
    for i in range(0, bufferSize):
        if not beatBuffer[i]==None:
            if beatBuffer[i]['id']==beat:
                beatBuffer[i]['chord']=getChordName(chord)
    print beatBuffer


def getChordName(chord):
    root = chord.root().name
    suffix = ""
    if chord.quality=="minor":
        suffix="m"
    elif chord.quality=="diminished":
        suffix="dim"
    return "{}{}".format(root, suffix)


def draw():
    '''Draws the window with the information given in pitchBuffer and beatBuffer'''
    global screen
    global width
    global height
    global font

    screen.fill((100, 100, 100))
    font = pygame.font.Font(None, 36)
    text = font.render(likelyChord, 1, (0, 0, 0))
    textpos = text.get_rect()
    textpos.x = width-32-textpos.width
    textpos.y=12
    screen.blit(text, textpos)

    text = font.render("aRHYTHMetica", 1, (0, 0, 0))
    textpos = text.get_rect()
    textpos.x = 32
    textpos.y=12
    screen.blit(text, textpos)

    font = pygame.font.Font(None, 24)
    drawScale()

    global bufferSize
    global pitchBuffer
    global graphBounds

    prevx=graphBounds.x
    xstep=graphBounds.width/bufferSize
    prevy=0
    for i in range(1, bufferSize):
        if not pitchBuffer[i]==None:
            if not pitchBuffer[i-1] == None:
                x = graphBounds.x + xstep*i
                y = pitchBuffer[i]
                prevx=x-xstep
                prevy = pitchBuffer[i-1]
                pygame.draw.line(screen, (255, 255, 0), (prevx, prevy), (x, y), 2)
        if not beatBuffer[i]==None:
            x = graphBounds.x + xstep*i
            pygame.draw.line(screen, (0, 0, 0), (x, graphBounds.y), (x, graphBounds.width), 1)
            text = font.render(str(beatBuffer[i]['chord']), 1, (0, 0, 0))
            textpos = text.get_rect()
            textpos.x = x - textpos.width
            textpos.y=graphBounds.y-textpos.height
            screen.blit(text, textpos)

    pygame.display.update()


def drawScale(bounds = graphBounds):
    global octaves
    octaveHeight = graphBounds.height/octaves
    for octave in range(0, octaves):
        drawPiano(pygame.Rect(bounds.x, bounds.y + octave*octaveHeight, bounds.width, octaveHeight));

def drawPiano(bounds=graphBounds):
    global screen
    global width
    global height
    global keyPattern
    for key in range(0, 12):
        starty=key*bounds.height/12+bounds.y

        colour = (255, 255, 255) if keyPattern[-key-1]==0 else (0, 0, 0)

        pygame.draw.rect(screen, colour, pygame.Rect(bounds.x-10, starty, 10, bounds.height/12+1))
        pygame.draw.line(screen, (0, 0, 0), (bounds.x, starty), (bounds.x+bounds.width, starty), 1)

def map(value, istart, istop, ostart, ostop):
    return ostart + (ostop - ostart) * ((value - istart) / (istop - istart))

def getCoordinate(linearPitch):
    if linearPitch == None: return None

    global pitchRange
    global pitchRangeCoordinates
    linearPitch = pitchRange[1] - (linearPitch - pitchRange[0])
    return map(linearPitch, pitchRange[0], pitchRange[1], pitchRangeCoordinates[0], pitchRangeCoordinates[1])
