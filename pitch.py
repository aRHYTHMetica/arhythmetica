import numpy
import math

# Fast Lifting Wavelet Transform Pitch Tracking
# http://courses.physics.illinois.edu/phys406/NSF_REU_Reports/2005_reu/Real-Time_Time-Domain_Pitch_Tracking_Using_Wavelets.pdf

# python 27 ne raboti s unikod :(

#Audio
CHANNELS = 1
RATE = 44100 		#Sample rate [Hz]
CHUNK = 1024 		#Buffer size (Should be a multiple of 2^FLWT_LEVELS)

#Wavelet pitch tracker
FLWT_LEVELS=6		#FLWT Levels
DIFF_LEVEL=3 		#Difference levels
EXT_THERSHOLD=0.65	#Extremum threshold
MAX_FREQ=3000		#Max frequency [Hz]
MIN_LOUDNESS=-24	#dB

amplitudeThreshold=0
pitch=0
pitchConfidence=-1
prevPitch = 0
data=numpy.random.rand(CHUNK)
noteFrequency = [16.352, 17.324, 18.354, 19.445, 20.602, 21.827, 23.125, 24.500, 25.957, 27.500, 29.135, 30.868] #used for pitchToTone()

noteCount = [0]*12
normalizedCount = []
localNoteCount = [0]*12
localNormalizedCount = []

pitchOffset=0
pitchLinearOffsets=[0]*27

hasTonicHistogram = False
tonicHistogramMinimum = 100

currentPitch = 0
currentPitchLinear = 0

def pitchDistance(pv, pc):
	'''Returns the ratio between the current pitch [pc] and the previous [pd].'''
	pv = float(pv)
	pc = float(pc)
	return abs(pv-pc) / pc


def getLinearPitch(p):
	'''
	Returns the distance between the pitch [p, Hz] and the A4 note (440.0Hz) in semitones.
		440Hz	 -> A  -> 0
		391.9Hz  -> G  -> -2
		266.2Hz  -> A# -> 1
	'''
	#http://www.phys.unsw.edu.au/jw/notes.html
	#We use A4 since its frequency easy to remember
	if p==0 or p==-1: return None
	return 12 * math.log(p/440.0, 2)


def getPitchOffset(p):
	'''Returns the distance to the nearest known note as semitones'''
	return round(p)-p


def getBestOffset(pitch):
	'''Using the recent pitches, getBestOffset() finds how much the pitches need to be offset in order to be as close to the 12 tones as possible.'''
	global pitchLinearOffsets
	global pitchOffset
	pitchLinearOffsets.pop(0)
	pitchLinearOffsets.append(getPitchOffset(pitch))

	pitchOffset = sum(pitchLinearOffsets) / len(pitchLinearOffsets)

	return pitchOffset


def getPitch(pitch, currentLoudness = MIN_LOUDNESS):
	'''Given a certain pitch, getPitch() compares it with the previous input and returns the most probable pitch.'''
	global prevPitch
	global pitchConfidence		#0: the pitch is non-sense; 1: the pitch is somewhat reliable, etc\ldots
	maxPitchConfidence = 4		#How many times getPitch() will return the previous pitch if getRawPitch()==0

	if not currentLoudness > MIN_LOUDNESS:
		pitch = -1


	#TODO: Find these citations that state human pitch can't double all of the sudden, etc. and implement them as a manner of correcting program mistakes
	#The following is similar to the postprocessing done in dywapitchtracker, only better

	if not pitch == 0 and not pitch == -1:						#Pitch is detected
		if prevPitch == 0 or prevPitch == -1:					#No previous pitch
			pitchConfidence=1
		elif pitchDistance(prevPitch, pitch)<0.2:
			pitchConfidence+=1
			pitchConfidence=min(pitchConfidence, maxPitchConfidence)
			pass
			#TODO: Rewrite the following mess
		elif pitchDistance(prevPitch, pitch/4)<0.2 and pitchConfidence>=maxPitchConfidence-2:
			pitch /= 4
		elif pitchDistance(prevPitch, pitch/2)<0.2 and pitchConfidence>=maxPitchConfidence-2:
			pitch /= 2
		elif pitchDistance(prevPitch, pitch/0.5)<0.2 and pitchConfidence>=maxPitchConfidence-2:
			pitch /= 0.5
		elif pitchDistance(prevPitch, pitch/0.25)<0.2 and pitchConfidence>=maxPitchConfidence-2:
			pitch /= 0.25
		else:
			#A very different value from prevPitch - nonsense
			if pitchConfidence>=1:		#If we are condfident enough in the previous pitch, take it
				pitch=prevPitch
				pitchConfidence-=1
			else:						#Otherwise, ignore prevPitch and return the current
				pitchConfidence=1
	else:
		if pitch == 0:									#There is no detected pitch
			if not (prevPitch == 0 or prevPitch == -1):		#But there was before
				if pitchConfidence>=1:					#If we are confident in the previous pitch, take it
					pitch=prevPitch
					pitchConfidence-=1
				else:									#Otherwise, return the nonsence you have
					pitch=-1
		else:											#Is no sound for sure
			pass
			#Return the pitch of silence

	prevPitch=pitch

	global currentPitchLinear
	currentPitchLinear = getLinearPitch(pitch)
	if not pitch == 0 and not pitch == -1:
		lPitch=getLinearPitch(pitch)

		lPitch+=getBestOffset(lPitch)
		currentPitchLinear=lPitch
		global pitchOffset
		#print linearPitchToTone(lPitch)

		addToNoteCount(linearPitchToTone(lPitch))


	return pitch


def getRawPitch():
	'''Estimates the pitch of the data supplied. May need post-proccessing'''
	#Described in that paper, see beginning
	global data

	dc = data.mean()

	amplitudeMax = max(abs(data.max()), abs(data.min()))
	peak=amplitudeMax
	amplitudeThreshold = amplitudeMax * EXT_THERSHOLD
	curlLevel = 0
	curlDistance = -1.0
	bufferSize = data.size

	data=data-dc #substract DC component

	while 1:
		minDelta = max(abs(RATE/(MAX_FREQ*(2**curlLevel))),1)	#(3)
		#print("Level", curlLevel, "delta", minDelta, "size", bufferSize)
		if bufferSize<2:	break

		#Find all local max and mins that
		# 1) Follow a zero-crossing and
		# 2) Are above amplitudeThershold and
		# 3) Are at least minDelta from the last extremum
		maxs=[]
		mins=[]
		findMax=0
		findMin=0
		lastMaxIndex=-1000
		lastMinIndex=-1000

		prevValueDelta=-1000
		for i in range(2, bufferSize):
			a=data[i]
			b=data[i-1]

			# Check if the line has gone through a zero-crossing
			if (b <= 0 and a > 0): findMax = 1
			if (b >= 0 and a < 0): findMin = 1

			valueDelta=a-b

			if prevValueDelta>-1000:	#not clever
				if findMin and prevValueDelta < 0 and valueDelta >= 0:
					#A local minimum has been reached
					if abs(a) >= amplitudeThreshold:
						if i - lastMinIndex > minDelta:
							mins.append(i)
							lastMinIndex = i
							findMin = 0
				if findMax and prevValueDelta > 0 and valueDelta <= 0:
					#A local maximum has been reached
					if abs(a) >= amplitudeThreshold:
						if i - lastMaxIndex > minDelta:
							maxs.append(i)
							lastMaxIndex = i
							findMax = 0

			prevValueDelta=valueDelta

		if len(mins)+len(maxs)<3:	break	#At least 2 items in either maxs or mins are required to proceed

		#Calculate mode distances

		distances = [0] * CHUNK

		for i in range(0, len(mins)):
			for j in range(1, DIFF_LEVEL):
				if i+j < len(mins):
					d = abs(mins[i] - mins[i+j])
					distances[d] = distances[d] + 1


		for i in range(0, len(maxs)):
			for j in range(1, DIFF_LEVEL):
				if i+j < len(maxs):
					d = abs(maxs[i] - maxs[i+j])
					distances[d] = distances[d] + 1

		#Find the peak window in distances[]
		#TODO: Try using numpy
		bestDistanceCount=-1
		bestDistanceWindow=-1

		for i in range(0, bufferSize):
			summed=0
			for j in range(i-minDelta, i+minDelta):
				if j>=0 and j<bufferSize:
					summed+=distances[j]
			if summed==bestDistanceCount:
				if i==2*bestDistanceWindow: bestDistanceWindow=i #Harmonic frequency
			else:
				if(summed>bestDistanceCount):
					bestDistanceCount=summed
					bestDistanceWindow=i

		#Calculate the weighted average
		averageDistance = 0
		averageCount = 0

		for i in range(bestDistanceWindow-minDelta, bestDistanceWindow+minDelta+1):
			if i>=0 and i<CHUNK:
				dist=distances[i]
				if dist>0:
					averageCount+=dist
					averageDistance+=i*dist

		averageDistance/=averageCount

		##

		if curlDistance > (-1.):
			similarity = abs(2*averageDistance -curlDistance)
			if similarity<=2*minDelta:
				pitchFreq=RATE/(2**(curlLevel-1)*curlDistance)
				return pitchFreq;


		curlDistance = averageDistance
		curlLevel+=1

		if curlLevel>FLWT_LEVELS: break
		if bufferSize<2:	break

		#Downsamle using FLWT - Apply wavelet transform
		bufferSize=bufferSize/2

		for i in range (0, bufferSize):
			data[i]=(data[2*i]+data[2*i+1])/2

	return 0


def linearPitchToTone(pitch):
	'''
	The function pitchToTone(pitch) takes one number -- a pitch(semitones relative to A4[440.0Hz]),
	and returns a tuple (octave, musical note), where the octave is
	denoted by numbers from 0 to 7, according to the Wikipedia article on
	Scientific Pitch Notation, and the musical note is denoted (pun intended)
	by numbers, such that:
	    C ->    0
	    C#->    1
	    D ->    2
	    D#->    3
	    E ->    4
	    F ->    5
	    F#->    6
	    G ->    7
	    G#->    8
	    A ->    9
	    A#->    10
	    B ->    11

	Reference: http://en.wikipedia.org/wiki/Scientific_pitch_notation#Table_of_note_frequencies
	'''
	#First, round the pitch and avoid silly python floating point division
	pitch=int(round(pitch))
	#Transpose the pitch up so it will match Scientific pitch notation
	pitch+=9
	#The octave realtive to A4. One octave is 12 semitones
	octave=pitch/12
	#Increase octave so it will match the desired notation. A4 is in octave 4, not 0
	octave+=4
	#Get the note only, dismissing the octave
	note = pitch%12
	#Return a tuple
	return (octave, note)


def pitchToTone(pitch):
    '''
    The function pitchToTone(pitch) takes one number -- a pitch(Hz),
    and returns a tuple (octave, musical note), where the octave is
    denoted by numbers from 0 to 7, according to the Wikipedia article on
    Scientific Pitch Notation, and the musical note is denoted (pun intended)
    by numbers, such that:
        C ->    0
        C#->    1
        D ->    2
        D#->    3
        E ->    4
        F ->    5
        F#->    6
        G ->    7
        G#->    8
        A ->    9
        A#->    10
        B ->    11

    Reference: http://en.wikipedia.org/wiki/Scientific_pitch_notation#Table_of_note_frequencies
    '''

    #Returning the tone sung as (octave, note) is better than MIDI
    #since even if getPitch got octave wrong, note will still be right:))

    #Furthermore, key-finging algorhythms (pun intended) do not take the octave of the note into account.
    #Discarding the octave in MIDI may be easy, but this is even simplier.

    global noteFrequency # the frequences of musical notes in Octave[0]

    pitch = int(pitch)		#WTF?
    #calculating the octave of the pitch:
    octave = 0
    while pitch>noteFrequency[len(noteFrequency)-1]:
        octave+=1
        pitch/=2

    #A=440Hz
    #440 would return 9 (A), but 441 would return 10 (A#)
    #TODO: find the closest mach in noteFrequency

    #calculating which note does the pitch stand for:
    note = 0
    for freq in noteFrequency:
        if not pitch>freq:
            note = noteFrequency.index(freq)
            break

    #TODO: What to return if pitch==-1 or pitch==0? None?

    #returning a tuple
    return (octave, note)


def addToNoteCount(tone):
	global noteCount
	global localNoteCount
	global hasTonicHistogram
	global tonicHistogramMinimum
	noteCount[tone[1]] +=1 #according to pitchToTone(), a tone is a tuple (octave, note)
	hasTonicHistogram = True if sum(noteCount) >tonicHistogramMinimum else False


	localNoteCount[tone[1]] +=1

def normalizePitchCount():
	global noteCount
	global normalizedCount
	normalizedCount = noteCount[:]
	sumOfNotes = sum(noteCount)
	sumOfNotes = max(1, sumOfNotes) #python is not any good at dividing by zero

	for count in normalizedCount:
		normalizedCount[normalizedCount.index(count)] = float(count/float(sumOfNotes))
	#noteCount = [0]*12 #clear the note count.

def normalizeLocalPitchCount(clear):
	global localNoteCount
	global localNormalizedCount
	localNormalizedCount = localNoteCount[:]
	sumOfNotes = sum(localNoteCount)
	sumOfNotes = max(1, sumOfNotes) #python is not any good at dividing by zero

	for count in localNormalizedCount:
		localNormalizedCount[localNormalizedCount.index(count)] = float(count/float(sumOfNotes))

	if clear: localNoteCount = [0]*12 #clear the note count.

	return localNormalizedCount



def startBeat():
	normalizeLocalPitchCount(True)

def getBeat():
	global localNoteCount
	global localNormalizedCount
	normalizeLocalPitchCount(False)
	beat = {'noteCount': sum(localNoteCount), 'pitchHistogram':localNormalizedCount, 'assignedChord':None}
	return beat


