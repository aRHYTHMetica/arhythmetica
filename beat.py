#   aRHYTHMetica
#   beat.py

#   Given sound data, beat.py analyses it on-line
#   and predicts when the next beat will occur

import time
import math
import numpy
import matplotlib.pyplot as mp
import matplotlib.mlab as mm


RATE = 44100 		#Sample rate [Hz]
CHUNK = 1024		#Window size
WINDOW_TIME = 5		#Seconds
WINDOW_CHUNKS = WINDOW_TIME * CHUNK / RATE
MIN_LOUDNESS=-27 #dB


features = [0] * WINDOW_CHUNKS
#Offset times are put in the queue 'beats'
loudness=[]
prevPitch = -1
isPause=True

pauseCounter = 0

def isLoud(levelLoudness):
	if levelLoudness > MIN_LOUDNESS:
		return True
	else:
		return False

def defineIsPause(levelLoudness):
	global pauseCounter
	global isPause
	if isPause:
		if isLoud(levelLoudness):
			isPause =  False #the pause has ended
	elif not isLoud(levelLoudness):
		isPause = True #a new pause has begun
		print "#"*50
		pauseCounter += 1




def pushData(ldnss, time):
	'''Tells beat.py what the sound loudness is, followed by the time the measurement was taken.'''
	#Checks if there is an offset time
	global loudness
	defineIsPause(ldnss)

	loudness.append(ldnss)
	pass


def rollingenvelope(position,width):
	global loudness
	if (position<loudness.size):
		envelopebuffer = []
		start = 0 if position-width<0 else (position-width) #handle the case where the envelope under runs the beginning of the file
		end = loudness.size if position+width > loudness.size else (position+width) #handle the case where the envelope overruns the end of the file
		for i in range(start,end):
			envelopebuffer.append(loudness[i])#grabs the decimal value of the current frame
		return numpy.average(sorted(envelopebuffer)[-(width/10):])
	else:
		print 'Position out of range'
		return 0

def finish():
	global loudness
	global pauseCounter
	print pauseCounter
	loudness=80+numpy.array(loudness)
	envelope = []
	for i in range(0, loudness.size):
		envelope.append(rollingenvelope(i, 10))
	#mp.plot(loudness)
	mp.hold('on')
	mp.plot(envelope, 'r')
	mp.savefig('hilbert.eps')
	mp.show()

def cleanup():
	'''Cleans up memory that contains data older tha 5 secs.'''
	pass
