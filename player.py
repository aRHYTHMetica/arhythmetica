#   aRHYTHMetica
#   player.py

#   Plays chords using MIDI.

#	Usage:
#		>>>import player
#		>>>from music21 import *
#		>>>tonality=key.Key('c', 'major')
#		>>>import key as kk
#		>>>player.playChord(kk.chords[0])
#		>>>player.playChord(kk.chords[6])
#		>>>player.playChord(kk.chords[4])
#		>>>player.playChord(kk.chords[5])


from time import sleep
import threading
import pygame
import pygame.midi
from music21 import chord, pitch, roman

#TODO: player.py should be able to change the chord in case of mistake

GRAND_PIANO = 0
HARP = 47
CHURCH_ORGAN = 19
TAP = 116
instrument = TAP

#Initialize pygame.midi
pygame.midi.init()

#On Windows, this links to a software synthesizer. On newer Macs, a third-party app like SimpleSynth may be needed
port = pygame.midi.get_default_output_id()
midi_out = pygame.midi.Output(port, 0)
midi_out.set_instrument(GRAND_PIANO, 0)
midi_out.set_instrument(TAP, 1)



class chordThread(threading.Thread):
	'''
	Plays a chord in its own thread.

	Arguments:
		chord:		music21.chord 		The chord to be played
		duration:	float 				The length of the chord in seconds
		arpeggio:	boolean 			If True, the chord will be broken with arpeggios
		staccato:	boolean				If True, the chord will be played staccato

	Usage:
		>>>chord = chordThread(chords[i-1], 1, True, False)
		>>>chord.start() # Play the chord in a separate thread
		>>>chord.join()  # (Optionally) Wait until the chord has completed

	'''
	def __init__(self, chord, duration, arpeggio, staccato):
		super(chordThread, self).__init__()
		self.chord=chord
		self.duration=duration
		self.staccato=staccato
		self.arpeggio=arpeggio

	def run(self):
		global midi_out
		if self.arpeggio:
			noteCount=len(self.chord.pitches)
			noteDelay=0.015
			duration = self.duration-noteDelay*noteCount
			for p in self.chord.pitches:
				midi_out.note_on(p.midi,80)
				#midi_out.note_on(p.midi-12,100)
				sleep(noteDelay)
			sleep(duration)
			for p in self.chord.pitches:
				midi_out.note_off(p.midi,80)
				#midi_out.note_off(p.midi-12,100)

		else:
			for p in self.chord.pitches:
				midi_out.note_on(p.midi,80)
				#midi_out.note_on(p.midi-12,100)
			if self.staccato:
				sleep(self.duration/3)
			else:
				sleep(self.duration)
			for p in self.chord.pitches:
				midi_out.note_off(p.midi,80)
				#midi_out.note_off(p.midi-12,100)

			if self.staccato: sleep(self.duration*2/3)


class drumThread(threading.Thread):
	'''
	Plays a tap sound in its own thread.

	Usage:
		>>>drum = drumThread()
		>>>drum.start() # Play the drum in a separate thread
		>>>drum.join()  # (Optionally) Wait until the drum has completed

	'''
	def __init__(self):
		super(drumThread, self).__init__()

	def run(self):
		global midi_out

		midi_out.note_on(69,80, 1)
		sleep(0.5)
		midi_out.note_off(69,80, 1)




def end():
	'''
	Ends the MIDI session. MUST be called at the end of the program
	'''
	global midi_out
	del midi_out
	pygame.midi.quit()

def beat(predictedLength):
	pass
def drum():
	#midi_out.set_instrument(TAP)
	drumT = drumThread()
	drumT.start()
def playChord(chord, join = False):
	'''
	Plays a specified chord denoted using Roman numerals.
	The key should be set using setKey()

	Arguments:
		chord:		music21.chord		The chord to be played
	'''
	midi_out.set_instrument(GRAND_PIANO)
	chordT = chordThread(chord, 0.6, False, False)
	chordT.start()
	if join: chordT.join()
