#   aRHYTHMetica
#   main.py

#   Draws sound data from either a microphone or a file,
#   analyses it real-time and (hopefully) generates a MIDI accompaniment.

import sys
import time
import math
import numpy
import scipy.io.wavfile
import pyaudio
import pygame

from music21 import *

#Use "import as" so our modules won't interfere with music21

import pitch    as pitchTracking
import beat     as beatTracking
import key      as keyAnalysis

import player

import pitchGraph

from config import *

keyAnalysis.keyMode=MODE

RECORD_POS=0
RECORD_BUFFER_COUNT=0

MIN_LOUDNESS=-24 #dB

if REALTIME:
    p = pyaudio.PyAudio()
    stream = p.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE,
                    input=True,
                    frames_per_buffer=CHUNK)
else:
    (RATE, RECORD_DATA) = scipy.io.wavfile.read(RECORD_FILENAME)
    RECORD_BUFFER_COUNT=RECORD_DATA.size/CHUNK

#Configure the pitch tracker
pitchTracking.CHUNK         = CHUNK
pitchTracking.RATE          = RATE
pitchTracking.MIN_LOUDNESS  = MIN_LOUDNESS

beatTracking.CHUNK         = CHUNK
beatTracking.RATE          = RATE
beatTracking.MIN_LOUDNESS  = MIN_LOUDNESS

pitchGraph.MIN_LOUDNESS  = MIN_LOUDNESS

readyToAccompany = False #=hasFirstMeasure and hasKey
hasFirstMeasure = False
secondMeasure=False
firstMeasureIndex = -1
hasKey = False

beats = [] #will contain a list of the beats that have already been analysed.
#Every beat is a dictonary which has a pitch histogram and the assigned chord

def loudness(chunk):
    '''Calculate and return volume of input samples

    Input chunk should be a numpy array of samples for analysis, as
    returned by sound card.  Sound card should be in 16-bit mono mode.
    Return value is measured in dB, will be from 0dB (maximum
    loudness) down to -80dB (no sound).  Typical very loud sound will
    be -1dB, typical silence is -36dB.

    '''
    #Borrowed from soundanalyse
    #Shouldn't this be terribly slow??
    data = numpy.array(chunk, dtype=float) / 32768.0
    ms = math.sqrt(numpy.sum(data ** 2.0) / len(data))
    if ms < 10e-8: ms = 10e-8
    return 10.0 * math.log(ms, 10.0)

def getAudio():
    '''Feeds an audio block either from the microphone or from a predefined
    file as a numpy array into global audioBuffer.

    The length of the returned buffer is defined by the global variable CHUNK.

    If the global variable 'REALTIME' is set to false, getAudio() will use data
    from a wav file. If the wav file has ended, getAudio() will return False.

    '''

    global audioBuffer
    global RECORD_DATA
    global RECORD_POS

    if REALTIME:
        audioString     = stream.read(CHUNK)
        audioBuffer     = numpy.fromstring(audioString,dtype=numpy.int16)
        return True
    else:
        if RECORD_POS<RECORD_BUFFER_COUNT:
            audioBuffer = numpy.split( RECORD_DATA,
                                      [RECORD_POS*CHUNK, (RECORD_POS+1)*CHUNK])
            audioBuffer = audioBuffer[1].astype(numpy.int16)
            RECORD_POS+=1
            return True
        else:
            return False

def getTempo():
    '''
    This function works as a tap button. It tracks Keypress events
    -- e.g., pressing Space bar, and sets the tempo of playChord() in player.py
    '''

    #Init a window so we can register events
    pygame.init()
    screen = pygame.display.set_mode((200,200))
    pygame.display.set_caption('Tap to set tempo')

    #Use different colours each beat
    colours = [(126, 11, 175), (200, 10, 0), (250, 240, 0), (0, 0, 128)]
    screen.fill(colours[0])
    pygame.display.update()

    startTime = -1
    endTime = -1
    counter = 0
    N = 4 #Require 4 taps for setting the tempo
    #TODO: move this to the Hamlet loop
    while True:
        event = pygame.event.wait() #just one event at a time
        if event.type == pygame.MOUSEBUTTONDOWN or event.type == pygame.KEYDOWN:
            if startTime == -1:
                startTime = time.time()
            counter +=1
            player.drum()
            if counter<len(colours):
                screen.fill(colours[counter])
                pygame.display.update()

        if counter == N:
            endTime = time.time()
            break
    allTime = endTime - startTime
    tempo = allTime / (N-1.0)
    pygame.display.quit() # so that the display does not stay there forever
    print tempo, allTime
    return tempo


#TODO: Implement pitchTracking tracking in C so it is faster

tempo =  getTempo() # we need to put this somewhere else

lastBeat = time.time() #Used for accurate measurement of time between beats

def newBeat():
    global lastBeat
    if time.time()-lastBeat>tempo:
        lastBeat=time.time()
        return True
    return False
def halfBeat():
    return True
    global lastBeat
    if time.time()-lastBeat>tempo/2:
        #lastBeat=time.time()
        return True
    return False

currentChord = 0
chordPlayed = False #was the chord played for this beat?

#screen = pygame.display.set_mode((600,480))
#pygame.display.set_caption('Histogram')
pitchGraph.init()

try:
    #Executed till a KeyboardInterrupt has occured.
    #This way we can stop the program safely
    while 1:
        try:
            #If we have reached the end of the audio file,
            #getAudio will return False and we no longer need to iterate.
            if getAudio():
                pass
            else:
                break

            pitchTracking.data   = audioBuffer
            currentLoudness      = loudness(audioBuffer)
            currentRawPitch      = float(pitchTracking.getRawPitch())
            #Convert to float, otherwise python will divide whole numbers
            currentPitch         = pitchTracking.getPitch(  currentRawPitch,
                                                            currentLoudness)


            # Key finding
            if pitchTracking.hasTonicHistogram and not hasKey and secondMeasure:
                #The key can be determined - do it!

                #Normalise the note distribution so sum(histogram)=1.0
                pitchTracking.normalizePitchCount()
                histogram=pitchTracking.normalizedCount
                likelyKeys=keyAnalysis.getKeys(histogram)

                print "Found key: {} {}".format(likelyKeys[MODEI][0][0].name, MODE)

                keyAnalysis.setKey(key.Key(likelyKeys[MODEI][0][0], MODE))
                #print "Most likely Minor key: {}".format(likelyKeys[1][:2]):
                hasKey = True

                #The player can switch from percussion to piano
                readyToAccompany = True
                player.instrument= player.GRAND_PIANO
                player.midi_out.set_instrument(player.instrument)

                #Look back and assign chords to the beats
                beats[0]['assignedChord']=0;

                for i in range(1, len(beats)):
                    #Use the chord form the previous beat
                    prevChord = beats[i-1]['assignedChord']
                    #Get the note distribution in the beat
                    beatHistogram = beats[i]['pitchHistogram']
                    #Find the likely chords based on the histogram
                    likelyChords = keyAnalysis.getLikelyChords(beatHistogram)
                    #Using the Markov chain, choose the most probable one
                    newChord = keyAnalysis.nextChord(prevChord, likelyChords)
                    #Assign the chord to the beat
                    beats[i]['assignedChord'] = newChord


            visualBeat = None
            if newBeat():   #There is a beat =>we should play a chord
                #if hasFirstMeasure:
                visualBeat = {'id':len(beats), 'chord':''}
                beat = pitchTracking.getBeat()
                beat['assignedChord']=currentChord;


                beats.append(beat)
                #print beat['pitchHistogram']

                secondMeasure=True
                #TODO: Auftakt zu vermeiden
                if beats[-1]['noteCount']>10 and not hasFirstMeasure:
                    hasFirstMeasure = True
                    firstMeasureIndex = len(beats)-1
                    #print "First measure"

                chordPlayed = False
                if readyToAccompany:
                    pass
                    player.drum()
                    pitchGraph.setChord(len(beats)-2, keyAnalysis.chords[currentChord])
                    #player.playChord(keyAnalysis.chords[currentChord])
                else:
                    pass
                    player.drum()

                pitchTracking.startBeat()

            if readyToAccompany:
                pitchHistogram = pitchTracking.normalizeLocalPitchCount(False)
                prevAssignedChord = beats[-1]['assignedChord']
                newLikelyChords = keyAnalysis.getLikelyChords(pitchHistogram)

                newChord = keyAnalysis.nextChord(prevAssignedChord, newLikelyChords)
                pitchGraph.likelyChord = pitchGraph.getChordName(keyAnalysis.chords[newChord])
                #tell player.py the chord
                if newChord != currentChord:
                    #player.playChord(keyAnalysis.chords[newChord])
                    pass
                if not chordPlayed:# and halfBeat():
                    if sum(pitchTracking.localNoteCount)>5:
                        chordPlayed = True
                        player.playChord(keyAnalysis.chords[newChord])
                currentChord = newChord
                #print currentChord
                #TODO: If currentChord changed, tell player.py we had a mistake
            pitchGraph.pushData(pitchTracking.currentPitchLinear, currentLoudness, visualBeat)
            if DEBUG:
                if currentLoudness<MIN_LOUDNESS:
                    print "..\t\t{}".format(currentLoudness)
                elif currentPitch==0:
                    print "##\t\t{}".format(currentLoudness)
                else:
                    if currentPitch == -1 or currentPitch == 0.0: currentPitch=" "
                    if currentRawPitch == -1 or currentRawPitch == 0.0: currentRawPitch=" "
                    print "{}\t{}\t{}".format(currentRawPitch, currentPitch, currentLoudness)
        except IOError, e:
            pass
except KeyboardInterrupt:
    #When we have shuffled off this mortal coil
    #Must give us pause

    pass

#pygame.display.quit()

if REALTIME:
    stream.stop_stream()
    stream.close()
    p.terminate()

print ""
print "Thanks for singing with aRHYTHMetica."
print ""

for chord in keyAnalysis.chords:
    print chord



#End of program
#TODO: Print chords used as pdf or whatever
player.end()
pitchGraph.end()
