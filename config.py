import pyaudio

DEBUG = False

#Microphone settings
CHUNK = 1024             #Feed the pitch tracker with blocks of 1024 samples
FORMAT= pyaudio.paInt16  #The pitch tracker loves this flavour of python ints
CHANNELS = 1             #We need only one channel audio, stereo is useless
RATE  = 44100            #Take 44100 samples per second from the microphone

#Accompaniment mode
MODE = 'minor'
MODEI= 1 #1 for minor scales, 0 for major



#File input
REALTIME = True
#A name of a wav sound recording to open if not REALTIME
#Windows
#RECORD_FILENAME="c:/users/valer_000/documents/arhythmetica/data.wav"
#UNIX-like OS
RECORD_FILENAME="wav/data.wav"

#The use of a sound file is confirmed to work with mono wav files

#In Audacity:
#   1) Make a recording
#   2) In 'Tracks'>'Stereo Track to Mono'
#   3) 'File'>'Export'

#For m4a files recorded on Windows:
#    #!/bin/bash
#    for f in *.m4a
#    do
#        name=`echo "$f" | sed -e "s/.mp4$//g"`
#        ffmpeg -i "$f" -vn -ar 44100 -ac 1 -ab 192k -f wav "$name.wav"
#    done
