#   aRHYTHMetica
#   key.py

#   Finds the key of the piece, the chords that need to be used.
#	Assesses chord likelihood for a given histogram and suggests progressions

from music21 import *	#Not recommended by PEP8, but it's fine here
from numpy.random import choice

keySignature = key.Key('c', 'major')
keyMode = '"major"'
chords = [] #used for storing the seven chords we need as music21.chord objects

#chords[0] -> I -> Tonic
#chords[3] -> IV-> Subdominant
#chords[4] -> V -> Dominant


class noteWeightAnalyser(analysis.discrete.KeyWeightKeyAnalysis):
	def __init__(self, referenceStream=None):
		analysis.discrete.KeyWeightKeyAnalysis.__init__(self,
											referenceStream=referenceStream)

	def _likelyKeys(self, pcDistribution):
		#pcDistribution = self._getPitchClassDistribution(sStream)
		#environLocal.printDebug(['process(); pcDistribution', pcDistribution])

		keyResultsMajor = self._convoluteDistribution(pcDistribution, 'major')
		differenceMajor = self._getDifference(	keyResultsMajor,
			         							pcDistribution, 'major')
		likelyKeysMajor = self._getLikelyKeys(keyResultsMajor, differenceMajor)


		keyResultsMinor = self._convoluteDistribution(pcDistribution, 'minor')
		differenceMinor = self._getDifference(	keyResultsMinor,
		                  						pcDistribution, 'minor')
		likelyKeysMinor = self._getLikelyKeys(keyResultsMinor, differenceMinor)

		return likelyKeysMajor, likelyKeysMinor


def getKeys(histogram):
	'''
	INPUT:
		histogram: 12-item array representing the distribution of pitches.
		The sum of the list should be 1.
	OUTPUT:
		[0]: List of likely major keys
		[1]: List of likely minor keys
		Each key is a tuple: (root note (pitch), likelihood coefficient)
	'''

	keyAnalyser = noteWeightAnalyser()
	probableKeys = keyAnalyser._likelyKeys(histogram)
	key = probableKeys
	return key


def setKey(newKey):
	'''
	Sets the key the piece is in. It should best be set when a few seconds of
	singing have already occurred. setKey() will alter the global 'chord' so
	the list contains the chords for that key.

	Arguments:
		key: 	music21.key 		The key
	'''

	global chords
	global keySignature
	global keyMode

	#newKey = key.Key('c', 'major');
	keySignature = newKey
	#TODO: change between major, harmonic minor and natural minor
	if keyMode == 'major':
		newKey = scale.MajorScale(newKey.pitchAndMode[0])
	elif keyMode == 'harmonicMinor' or keyMode == 'minor':
		newKey = scale.HarmonicMinorScale(newKey.pitchAndMode[0])
	elif keyMode == 'naturalMinor':
		newKey = scale.MinorScale(newKey.pitchAndMode[0])
	#roman.RomanNumeral(chord, key="Cmaj") returns a music21.chord from key

	chords = map(lambda d: roman.RomanNumeral(d+1, newKey), range(0, 7))
	#chords[2]=chords[2].closedPosition(forceOctave=3)
	for i in range(0, 7):
		chord = roman.RomanNumeral(i+1, newKey)

		if keyMode == 'minor' or keyMode == 'harmonicMinor':
			if chord.quality=='augmented':
				sixth = newKey.pitches[6].pitchClass
				for i in range(0, len(chord.pitches)):
					if chord.pitches[i].pitchClass == sixth:
						chord.pitches[i].accidental = None
						#print chord
		chords[i]=chord.closedPosition(forceOctave=3)


	#TODO: Fix augmented chords in harmonic minor!!
	#for chord in chords: print chord.quality, chord.pitches


def getLikelyChords(pitchHistogram):
	'''
	Returns the likelihood for each chord in the key in a 7-element tuple.
	Likelihood is in range [0; 1].
	'''

	global keySignature
	global chords
	likelyChords =[0]*7
	for chordNumeral in range(0, 7):
		chord = chords[chordNumeral]
		likelyChords[chordNumeral] = chordLikelihood(chord, pitchHistogram)

	return likelyChords


def chordLikelihood(chord, chordHistogram):
	'''
	Gives the likelihood of a chord to a pitch histogram.
	'''
	chordNotes = getChordNotes(chord)

	product = map(lambda i: chordNotes[i]*chordHistogram[i], range(0, 12))

	return sum(product)


def getChordNotes(chord):
	'''
	Given a chord, getChordNotes will return a list with 12 elements, which
	indicate where the chord notes are placed.
	'''
	tones = [0]*12
	for pitch in chord.pitches:
		tones[pitch.pitchClass]=1
	return tones


def getChordLikelihood(pitchHistogram):
	'''
	Given a pitch histogram, getChordLikelihood will compare it with the chords
	in global 'chords' and will return a tuple containing the likelihood for
	each chord.
	'''
	global chords
	likelyKeys = getKeys(pitchHistogram)
	likelihood = [0]*7


	for d in range(0, 7):
		chord = chords[d]

		#likelyKeys contains two lists of tuples (4 major & minor), where [0] is music21.pitch.Pitch and [1] is a float

		quality = 0 #we will use likelyKeys[quality]
		if chord.quality is 'major':
			#the chord is major => use likelyKeys[0]
			pass
		elif chord.quality is 'minor' or chord.quality is 'diminished':
			#the chord is either minor or dim.
			#Since a diminished chord is a minor with a lowered fifth, we threat it as minor
			quality = 1

		root = chord.root()

		for k in likelyKeys[quality]:
			if k[0].name==root.name:
				likelihood[d]=k[1]
		#now return the second item in the tuple that contains 'root' as the first item. Use likelyKeys[quality]



	return likelihood

#========================
#	CHORD PROGRESSIONS

transitionMatrix = [
	#I      ii      iii     IV      V       vi      vii
	[0.20, 	0, 		0.00,	0.20,	0.20,	0.20, 	0],			#I
	[0,		0,		0,		0, 		0.5, 	0, 		0.5],		#ii
	[0, 	0, 		0, 		0, 		0, 		1, 		0],			#iii
	[0.25, 	0.25, 	0, 		0, 		0.25, 	0, 		0.25],		#iV
	[0.4, 	0, 		0, 		0, 		0.3, 	0.3, 	0],			#V
	[0.0, 	0.5,	0, 		0.5, 	0, 		0, 		0],			#vi
	[0.5, 	0, 		0, 		0, 		0.5, 	0, 		0]			#vii
	]

chordTranstitions = [ [0]*7 ]*7

def nextChord(lastChord = 1, chordLikelihood = [0.143]*7):
    '''
    This function returns a number between 0 and 6, which stands for the most
    probable value of the next chord
    0<= int lastChord <=6, where lastChord is the last chord we have played
    chordLikelihood = (l_0, l_1, .. l_6), where l_i is float and
    l_0 + l_1 + .. + l_6 = 1

    The transition matrix we use is from
    http://impromptu.moso.com.au/tutorials/making_music/Diatonic%20Harmony.html
    '''

    global transitionMatrix

    ans = (-1) #??
    for i in range(0, 7):
    	chordLikelihood[i]+=1.0
    	chordLikelihood[i]*=float(transitionMatrix[lastChord][i])

    bestEl = max(chordLikelihood)


    ans = chordLikelihood.index(bestEl)
    #print "({}) -> ({})".format(lastChord, ans)
    return ans


def predictNextChord():

	pass
